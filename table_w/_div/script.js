// Data
const data = [
  {
    id: 1,
    column2: `${Math.random() * 1000}`,
    column3: `${Math.random() * 1000}`,
    price: `${Math.random() * 1000}.${Math.random() * 1000}`,
  },
  {
    id: 2,
    column2: `${Math.random() * 1000}`,
    column3: `${Math.random() * 1000}`,
    price: `${Math.random() * 1000}.${Math.random() * 1000}`,
  },
  {
    id: 3,
    column2: `${Math.random() * 1000}`,
    column3: `${Math.random() * 1000}`,
    price: `${Math.random() * 1000}.${Math.random() * 1000}`,
  },
  {
    id: 4,
    column2: `${Math.random() * 1000}`,
    column3: `${Math.random() * 1000}`,
    price: `${Math.random() * 1000}.${Math.random() * 1000}`,
  },
  {
    id: 5,
    column2: `${Math.random() * 1000}`,
    column3: `${Math.random() * 1000}`,
    price: `${Math.random() * 1000}.${Math.random() * 1000}`,
  },
  {
    id: 6,
    column2: `${Math.random() * 1000}`,
    column3: `${Math.random() * 1000}`,
    price: `${Math.random() * 1000}.${Math.random() * 1000}`,
  },
  {
    id: 7,
    column2: `${Math.random() * 1000}`,
    column3: `${Math.random() * 1000}`,
    price: `${Math.random() * 1000}.${Math.random() * 1000}`,
  },
  {
    id: 8,
    column2: `${Math.random() * 1000}`,
    column3: `${Math.random() * 1000}`,
    price: `${Math.random() * 1000}.${Math.random() * 1000}`,
  },
  {
    id: 9,
    column2: `${Math.random() * 1000}`,
    column3: `${Math.random() * 1000}`,
    price: `${Math.random() * 1000}.${Math.random() * 1000}`,
  },
];
// Some DOM elements that will be used a lot by functions.
var sortableElem = document.getElementById("sortable");
var columnHead = document.querySelectorAll("th[order]");

// Column "head" that is target of current sort.
// It's th element with all attributes. order, s-col
// Get default order element.
var current_sort_column =
  document.querySelector('th[order="bs"]') ||
  document.querySelector('th[order="sb"]');

// Add data to table. Also saves created tr element to data.
for (var i = 0; i < data.length; i++) {
  var tr = document.createElement("tr");
  sortableElem.appendChild(tr);
  // Save created dom element. Reordering will be done using this.
  data[i].dom = tr;

  // Columns
  var c1 = document.createElement("td");
  c1.textContent = data[i].id;
  var c2 = document.createElement("td");
  c2.textContent = data[i].column2;
  var c3 = document.createElement("td");
  c3.textContent = data[i].column3;
  var c4 = document.createElement("td");
  c4.textContent = data[i].price;
  // Add columns to this row.
  tr.appendChild(c1);
  tr.appendChild(c2);
  tr.appendChild(c3);
  tr.appendChild(c4);
}

// Add eventlistener to tables head.
// Clicked element will sort table using its "order" and "s-col" value.
for (i = 0; i < columnHead.length; i++) {
  columnHead[i].addEventListener(
    "click",
    function () {
      var order = this.getAttribute("order");
      if (order === "bs" || order === "no") {
        this.setAttribute("order", "sb");
        this.classList.toggle("sb");
        data.sort(sortSB(this.getAttribute("s-col")));
        reorderDOM();
      } else if (order === "sb") {
        this.setAttribute("order", "bs");
        this.classList.toggle("bs");
        data.sort(sortBS(this.getAttribute("s-col")));
        reorderDOM();
      }
      if (current_sort_column != null && current_sort_column != this) {
        current_sort_column.setAttribute("order", "no");
      }
      current_sort_column = this;
    },
    false
  );
}

function reorderDOM() {
  for (var i = 0; i < data.length; i++) {
    sortableElem.appendChild(data[i].dom);
  }
}

function sortSB(column) {
  return function (a, b) {
    if (a[column] < b[column]) {
      return -1;
    }
    if (a[column] > b[column]) {
      return 1;
    }
    return 0;
  };
}

function sortBS(column) {
  return function (a, b) {
    if (a[column] < b[column]) {
      return 1;
    }
    if (a[column] > b[column]) {
      return -1;
    }
    return 0;
  };
}
