const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const cors = require("cors");

const admin = require("firebase-admin");
const serviceAccount = require("./serviceAccountKey.json");

const indexRouter = require("./routes/index");
const usersRouter = require("./routes/users");

const app = express();

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://reactnetflix-1234-default-rtdb.firebaseio.com",
});

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(cors());

app.use("/", indexRouter);
app.use("/users", usersRouter);

app.post("/getmovies", (req, res, next) => {
  const { movieType } = req.body;
  const leafRoot = "movies";
  const movieRef = admin.database().ref(`${leafRoot}/${movieType}`);

  const moviesPromise = new Promise((resolve, reject) => {
    let moviesData = [];
    movieRef.on("value", async (snapshot) => {
      const movies = await snapshot.val();
      if (movies && movies.length !== 0) {
        movies.map((element, index) => {
          moviesData.push(element);
        });
        resolve(moviesData);
      } else {
        reject("Empty Data");
      }
    });
  });

  moviesPromise
    .then((value) => {
      res.status(201).json({ data: value });
    })
    .catch((error) => {
      console.log({ error });
      res.status(201).json({ msg: error });
    });
});

module.exports = app;
