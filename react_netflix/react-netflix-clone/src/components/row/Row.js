import { useEffect, useState } from "react";

function Row(props) {
  const [movies, setMovies] = useState([]);

  const { title, movieType, onMovieSelected } = props;

  const leafRoot = "movies";

  useEffect(() => {
    fetchMovies(movieType);
  }, []);

  const fetchMovies = (movieType) => {
    fetch("http://localhost:8888/getmovies", {
      method: "POST",
      body: JSON.stringify({ movieType: movieType }),
      headers: new Headers({
        "Content-Type": "application/json",
      }),
    })
      .then((res) => res.json())
      .catch((error) => console.error("Error:", error))
      .then((response) => response.data)
      .then((data) => setMovies(() => data));
  };

  const onMovieClicked = (movie) => {
    onMovieSelected(movie);
  };

  return (
    <div className="row">
      <h2>{title}</h2>
      <div className="row__posters">
        {movies.map((movie, index) => (
          <img
            key={index}
            onClick={() => onMovieClicked(movie)}
            className="row__poster row__posterLarge"
            src={`https://image.tmdb.org/t/p/original/${movie.poster_path}`}
            alt={movie.original_name}
          />
        ))}
      </div>
    </div>
  );
}

export default Row;
