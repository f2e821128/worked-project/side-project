$(document).ready(function () {
  $("#rpa-telegram-bot-tabs").each(function () {
    var $active,
      $content,
      $links = $(this).find("a");

    $active = $($links[0]);
    $active.addClass("active");

    $content = $($active[0].hash);

    $links.not($active).each(function () {
      $(this.hash).hide();
    });

    $(this).on("click", "a", function (e) {
      $active.removeClass("active");
      $content.hide();

      $active = $(this);
      $content = $(this.hash);

      $active.addClass("active");
      $content.show();

      e.preventDefault();
    });
  });
});

// Dropdown Javascript BEGIN
const linkToggle = document.querySelectorAll(".custom-select");

for (let i = 0; i < linkToggle.length; i++) {
  linkToggle[i].addEventListener("click", function (event) {
    event.preventDefault();
    const container = document.getElementById(this.dataset.container);

    const dataset = document.querySelectorAll("div[data-container]");
    for (let j = 0; j < dataset.length; j++) {
      const options = document.getElementById(dataset[j].dataset.container);
      if (options.id !== container.id) {
        options.style.display = "none";
        options.style.height = "0px";
        options.classList.remove("active");
      }
    }

    if (!container.classList.contains("active")) {
      this.classList.add("active");
      container.classList.add("active");
      container.style.height = "auto";
      container.style.display = "block";
      let height = container.clientHeight + "px";
      container.style.height = "0px";
      container.style.margin = "0 0 32px";
      setTimeout(function () {
        container.style.height = height;
        container.style.display = "block";
      }, 0);
    } else {
      this.classList.remove("active");
      container.style.display = "none";
      container.style.height = "0px";
      container.classList.remove("active");
    }
  });
}

// lister which user choose.
for (const option of document.querySelectorAll(".select-options li")) {
  option.addEventListener("click", function () {
    this.parentNode.parentNode.querySelector(
      ".custom-select span"
    ).textContent = this.textContent;
  });
}

window.onclick = function (event) {
  if (!event.target.matches(".option-selected")) {
    var dropdowns = document.getElementsByClassName("select-options");

    // Remove child active status.
    for (let i = 0; i < linkToggle.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains("active")) {
        openDropdown.style.display = "none";
        openDropdown.style.height = "0px";
        openDropdown.classList.remove("active");
      }
    }

    // Remove active when user click outside 
    for (let i = 0; i < linkToggle.length; i++) {
      linkToggle[i].classList.remove("active");
    }
  }
};
// Dropdown Javascript END
