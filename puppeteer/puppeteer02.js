const fs = require("fs");
const puppeteer = require("puppeteer");

const readingFunction = (filename, path, keyName) => {
  let jsonData = [];
  try {
    const data = fs.readFileSync(`${path}/${filename}`, "utf8");

    let spiltData = data.split("\n");
    spiltData.forEach((element) => {
      let eachData = element.split(". ");
      let obj = {};
      obj[keyName] = eachData[1];
      jsonData.push(obj);
    });
  } catch (err) {
    console.error(err);
  }

  return jsonData;
};

const comprehensive = readingFunction(
  "comprehensive.txt",
  ".",
  "comprehensive"
);
const css = readingFunction("css.txt", ".", "css");
const freecodecamp = readingFunction("freecodecamp.txt", ".", "freecodecamp");
const rn = readingFunction("rn.txt", ".", "rn");

const combine1 = comprehensive.concat(css);
const combine2 = freecodecamp.concat(rn);
const combine3 = combine1.concat(combine2);

var request = require("request");
var cheerio = require("cheerio");
const db = require("./firebase");

const { insertToDB, fetchFromDB } = db;

// insertToDB();
fetchFromDB();

function fetchTitle(url, onComplete = null) {
  request(url, function (error, response, body) {
    var output = url; // default to URL
    let obj = {};

    if (!error && response.statusCode === 200) {
      var $ = cheerio.load(body);
      // console.log(`URL = ${url}`);

      var title = $("head > title").text().trim();
      // console.log(`Title = ${title}`);

      output = `[${title}](${url})`;

      obj.linkTitle = title;
      obj.linkurl = url;
    } else {
      obj.errorMsg = error;
      obj.errorCode = response.statusCode;
      obj.erroruri = url;
      // console.log(`Error = ${error}, code = ${response.statusCode}`);
    }

    // console.log(`output = ${output} \n\n`);
    insertToDB("bookmark", obj);

    // if (onComplete) onComplete(output);
  });
}

function main() {
  console.log("START");

  combine3.forEach((element) => {
    const uri = Object.values(element)[0];
    fetchTitle(uri);
  });

  console.log("END");
}

// main();
