const admin = require("firebase-admin");

const serviceAccount = require("./serviceAccountKey.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL:
    "https://charles-bookmark-default-rtdb.asia-southeast1.firebasedatabase.app",
});

const db = admin.database();

const fetchFromDB = async () => {
  const ref = db.ref("bookmark");
  ref.once("value", function (snapshot) {
    const data = snapshot.val();
    // for (const [key, value] of Object.entries(data)) {
    //   console.log(`${key}: ${value}`);
    // }
    Object.entries(data).map((item, index) => {
      console.log(`${index} => ${item}`);
    });
    // console.log(`Data Length => ${data.length}`);
  });
};

const insertToDB = (path, data) => {
  //   const ref = db.ref("server/saving-data/fireblog/posts");
  const ref = db.ref(`${path}`);

  //   const usersRef = ref.child("users");

  ref.push({
    data,
  });
};

module.exports = { insertToDB, fetchFromDB };
