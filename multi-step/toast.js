// when dom content is loaded
document.addEventListener("DOMContentLoaded", (event) => {
  // get all elements with .toast-container class
  var toastContainer = document.querySelectorAll(".rpa__toast__container");
  // check if container already exist and add it if it doesen't
  if (toastContainer.length == 0) {
    // prepare toast-container element
    var toastContainerContent = '<div class="rpa__toast__container"></div>';
    // add it to the end of the body
    document.querySelector("body").innerHTML += toastContainerContent;
  }
});

// function for creating toast elements available with parameters
function createToast(type, title, text, duration) {
  // Creating toast message container as dom element
  let toastElem = document.createElement("div");
  // Adding toast class to it
  toastElem.classList.add("toast");
  // If there is a type, add that type name as class to toast message container
  if (type) {
    toastElem.classList.add(type);
  }

  // create title dom element
  let titleElem = document.createElement("p");
  // add rpa__toast__title class to doom element
  titleElem.classList.add("rpa__toast__title");

  // appent icon to title element with title text
  titleElem.innerHTML = title;
  toastElem.appendChild(titleElem);

  // create close element with rpa__toast__close class for closing the toast message
  let closeElem = document.createElement("p");
  closeElem.classList.add("rpa__toast__close");
  closeElem.setAttribute("onclick", "closeToast(this)");
  toastElem.appendChild(closeElem);

  // create text element with rpa__toast__text class and appent text to it
  let textElement = document.createElement("p");
  textElement.classList.add("rpa__toast__text");
  textElement.innerHTML = text;
  toastElem.appendChild(textElement);

  // get toast-container element
  let toastContainer = document.querySelector(".rpa__toast__container");

  //appent toast message to it
  toastContainer.appendChild(toastElem);

  // wait just a bit to add active class to the message to trigger animation
  setTimeout(function () {
    toastElem.classList.add("active");
  }, 1);

  // check duration
  if (duration > 0) {
    // it it's bigger then 0 add it
    setTimeout(function () {
      toastElem.classList.remove("active");
      setTimeout(function () {
        toastElem.remove();
      }, 350);
    }, duration);
  } else if (duration == null) {
    //  it ther isn't any add default one (3000ms)
    setTimeout(function () {
      toastElem.classList.remove("active");
      setTimeout(function () {
        toastElem.remove();
      }, 350);
    }, 3000);
  }
  //if duration is 0, toast message will not be closed
}

//addEventListener on mouse click for standard closing of toast message on right top "x"
document.addEventListener("click", function (e) {
  //check is the right element clicked
  if (!e.target.matches(".t-close")) return;
  else {
    //get toast element
    let toastElement = e.target.parentElement;
    // remove active class from it to trigger css animation with duration of 300ms
    toastElement.classList.remove("active");
    //wait for 350ms and then remove element
    setTimeout(function () {
      toastElement.remove();
    }, 350);
  }
});

function closeToast(event) {
  //   console.log(event.parentElement);
  let toastElement = event.parentElement;
  toastElement.classList.remove("active");

  setTimeout(function () {
    toastElement.remove();
  }, 350);
}

function defaultToast(title, text, duration = 1000) {
  title
    ? createToast("", "Default Toast", text, duration)
    : createToast("", "", text, duration);
}

function systemToast(title, text, duration = 1000) {
  title
    ? createToast("system", "System Toast", text, duration)
    : createToast("system", "", text, duration);
}

function successToast(title, text, duration = 1000) {
  title
    ? createToast("success", "Success Toast", text, duration)
    : createToast("success", "", text, duration);
}

function warningToast(title, text, duration = 1000) {
  title
    ? createToast("warning", "Warning Toast", text, duration)
    : createToast("warning", "", text, duration);
}

function bugToast(title, text, duration = 1000) {
  title
    ? createToast("bug", "Bug Toast", text, duration)
    : createToast("bug", "", text, duration);
}

function infoToast(title, text, duration = 1000) {
  title
    ? createToast("info", "Info Toast", text, duration)
    : createToast("info", "", text, duration);
}
