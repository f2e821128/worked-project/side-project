// Dropdown Checkbox BEGIN
(function ($) {
  var RPACheckboxDropdown = function (el) {
    var _this = this;
    this.isOpen = false;
    this.areAllChecked = false;
    this.$el = $(el);
    this.$label = this.$el.find(".rpa__Dropdown__Label");
    this.$checkAll = this.$el.find('[data-toggle="check-all"]').first();
    this.$inputs = this.$el.find('.rpa__Checkbox__Input[type="checkbox"]');

    this.$checkedString = this.$el.find(".rpa__Dropdown__Input");

    const rpaCheckboxChecked = this.$checkedString
      .prop("value")
      .trim()
      .split(",");

    let checkbox_Input_Array = document.querySelectorAll(
      ".rpa__Checkbox__Input"
    );

    for (let i = 0; i < checkbox_Input_Array.length; i++) {
      if (rpaCheckboxChecked.includes(checkbox_Input_Array[i].value)) {
        checkbox_Input_Array[i].checked = true;
      }
    }

    this.rpaOnCheckBox();

    this.$label.on("click", function (e) {
      e.preventDefault();
      _this.rpaToggleOpen();
    });

    this.$checkAll.on("click", function (e) {
      e.preventDefault();
      _this.rpaOnCheckAll();
    });

    this.$inputs.on("change", function (e) {
      _this.rpaOnCheckBox();
    });
  };

  RPACheckboxDropdown.prototype.rpaOnCheckBox = function () {
    this.rpaUpdateStatus();
  };

  RPACheckboxDropdown.prototype.rpaUpdateStatus = function () {
    var checked = this.$el.find(":checked");

    this.areAllChecked = false;
    this.$checkAll.html("Check All");

    let inputStr = "";
    for (let i = 0; i < checked.length; i++) {
      inputStr += `${checked[i].value},`;
    }

    this.$checkedString.prop("value", inputStr);

    if (checked.length <= 0) {
      this.$label.html("請設定權限");
    } else if (checked.length === 1) {
      // this.$label.html(checked.parent("label").text());
      this.$label.html("已選 " + checked.length + " 權限");
    } else if (checked.length === this.$inputs.length) {
      this.$label.html("All Selected");
      this.areAllChecked = true;
      this.$checkAll.html("Uncheck All");
    } else {
      this.$label.html("已選 " + checked.length + " 權限");
    }
  };

  RPACheckboxDropdown.prototype.rpaOnCheckAll = function (checkAll) {
    if (!this.areAllChecked || checkAll) {
      this.areAllChecked = true;
      this.$checkAll.html("Uncheck All");
      this.$inputs.prop("checked", true);
    } else {
      this.areAllChecked = false;
      this.$checkAll.html("Check All");
      this.$inputs.prop("checked", false);
    }

    this.rpaUpdateStatus();
  };

  RPACheckboxDropdown.prototype.rpaToggleOpen = function (forceOpen) {
    var _this = this;

    if (!this.isOpen || forceOpen) {
      this.isOpen = true;
      this.$el.addClass("on");
      $(document).on("click", function (e) {
        if (!$(e.target).closest("[data-control]").length) {
          _this.rpaToggleOpen();
        }
      });
    } else {
      this.isOpen = false;
      this.$el.removeClass("on");
      $(document).off("click");
    }
  };

  const rpaCheckboxesDropdowns = document.querySelectorAll(
    '[data-control="rpa-Checkbox-Dropdown"]'
  );

  for (var i = 0, length = rpaCheckboxesDropdowns.length; i < length; i++) {
    new RPACheckboxDropdown(rpaCheckboxesDropdowns[i]);
  }
})(jQuery);
// Dropdown Checkbox END
