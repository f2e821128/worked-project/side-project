### SCSS convert to CSS

#### Use Node

```
npm install -g sass
```

#### Use Chocolatey package manager

```
choco install sass
```

#### Homebrew

```
brew install sass/sass/sass
```

#### After install

```
sass public/css/style.scss public/css/style.css
```
