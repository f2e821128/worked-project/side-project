const fetch = require("node-fetch");
const cheerio = require("cheerio");

const fetchTitle = (url) => {
  const parseTitle = (body) => {
    const $ = cheerio.load(body);
    const title = $("head > title").text().trim();
    console.log({ title });

    return title;
  };

  return fetch(url)
    .then((response) => response.text()) // parse response's body as text
    .then((body) => parseTitle(body)) // extract <title> from body
    .then((title) => title) // send the result back
    .catch((e) => e.message); // catch possible errors
};

module.exports = fetchTitle;
