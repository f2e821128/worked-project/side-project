const db = require("../config/firebase.config");

const insertData = (path, data) => {
  console.log({ path });
  console.log({ data });
  const ref = db.ref(`${path}`);

  ref.push({
    data,
  });
};

module.exports = insertData;
