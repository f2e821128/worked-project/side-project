const admin = require("firebase-admin");

const serviceAccount = {
  type: "service_account",
  project_id: "gh-seo-test",
  private_key_id: "e4d600c93c40f5bceaf12b0439fa0edba936cb60",
  private_key:
    "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQD3bmH/UxcJYHlO\nndmEOB6EYESxKHOOmc372kp9dXfy6HVlRj9QKPup78p0oLxMUwnzWhcHc2bfVgIz\nq7Kp4E8gd/9eG57ue5ws/lfcfc3wugFqPEg9sKwjpXBdtbs9pgPLqRLrd603+zBY\ncFe64eqRpf9Yc/IlaryQa/QPlMwBmeozZLAH8HXDvtCJccV/16E+gQbtTn7eid7e\nXSsI/kbEh+RZmlz+3w2KQ4ohWNQ0kGKbeYZo6T1whRHp62U5rLVjk58zDXZrdLlL\nVc+wYXJfshB4zf//XlY8Jw+u9+74ziN+iciEo+Rvamf5tcQRObnlEgAdUmwsceEY\nIReJuM6VAgMBAAECggEAB+Ec/8t+lU7cLhO6xIog9qg2J8oY+/aL57RI4PvnqPWc\nH3CAPaZAdWXsdZBUzr1qOLSqvRMAuKX9hfW2x9Ab1PXdTPrmMcP/8jLbS2s6LqtN\ntjtbNZO1t6DiFineX5U007x7nMH/4yN9o6NYN9xP4vl9SyzODkD65kJ50DE12esj\n0Yi/yd2mXc04ZWfg3gIB4E/PUtJckAubedN6AJqMXsoex9gxG9sl5C+bATfg+efy\n8ZoQH0YbqRCGDQPGkjk2jsSmVtBPpcbveFLpYd/WoZDTaGcSEU5AteaFLYNj7dC9\nwzIX/SqLs38Ply2zH+g5ylO+vip0LPvt7jnkj0LpFQKBgQD9D3Mjjh1nrnKk/GHu\nS7Qe9jm8tvw/dmP/KkNRZK1tzZKtknrVSJB+U/GuvrUYvqmvvcUk1SLEsfFsfVsQ\nJLeRujaeaLDigLqvJ/wIQOQ3wr29BMoAxdtPqwCJbhX0pNu6vLXKdwPG+Wn780ty\nx6PbC+GrOvFWXB+41n0zaQ4HEwKBgQD6TjFqylxpC2mV0vJeJezQoCT9GgZFfXh8\nStxqnvlEvrewdwbhX6ysfOnQfWUG7OT/9EYj1fxUSsJd96+pmPh6YyyJoHQSnjxH\nUM/wzK/TxWCkQsTEbCH/ENDFyhL9PyJsMxCQ2axGbPQlGIizd4qfXPyucKU+m3CW\nopEG8f9AtwKBgBN+8jbmBvvq46g0TXMnX+wWuiIA0vQssc2Buwf1Qqs38DnxFAeE\nfKVO4Eq/JIKQhdAlRZ/tz6zprJjYG4Dl40KV3wlbNW1tz3QZF4CnRy3IkgeS6+Ed\nXSUJHpR357ceUFNVoIfFnYPhyTmA5+oi2UvR4YPomZk2pbb+x0yDpnIDAoGBAJyn\n/aPBnQ6dduqvSMOF5u43nI7QD+bD/XmE/Oi6MHZHCv4lsKezUoXt9ARpxqYL89MB\n1jgtgCxdUcKv90AlHjazBTfptVvFryJPQKWL5eubl72WkEIDD0/s/U0dCL95GRkw\n6IhPw9Ob8z2YWk5AXgEZmIewtqs1a8TLYAtBwCiHAoGBAM5+j99eFupa+Yo/yF5L\nag2vvfTW53QUSP9xmWuU+VYFnEacIMT1tfkcSEFS0sd8YqW04tS+9Ofp2lR/GMIP\nj0vIw5o1o6YRvhCKh0Wk2F7brQwMq0hr7XY7NhT1lJCf99gTE8QHLH6p9uJSll0R\nquYCafND1ac0kC9GSyydR+YU\n-----END PRIVATE KEY-----\n",
  client_email: "firebase-adminsdk-ct6ku@gh-seo-test.iam.gserviceaccount.com",
  client_id: "106797072932976390325",
  auth_uri: "https://accounts.google.com/o/oauth2/auth",
  token_uri: "https://oauth2.googleapis.com/token",
  auth_provider_x509_cert_url: "https://www.googleapis.com/oauth2/v1/certs",
  client_x509_cert_url:
    "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-ct6ku%40gh-seo-test.iam.gserviceaccount.com",
};
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL:
    "https://gh-seo-test-default-rtdb.asia-southeast1.firebasedatabase.app",
});

const db = admin.database();

module.exports = db;
