const express = require("express");
const db = require("../config/firebase.config");

const router = express.Router();

/* GET Manage Dashboard page. */
router.get("/bookmark", async function (req, res, next) {
  const ref = db.ref("bookmark");
  const bookmarkList = new Promise((resolve, reject) => {
    var bookmarkArray = [];
    ref.once("value", async (snapshot) => {
      const data = await snapshot.val();
      Object.entries(data).map((item, index) => {
        let obj = {};
        obj.key = item[0];
        obj.title = item[1].data.linkTitle;
        obj.url = item[1].data.linkurl;

        bookmarkArray.push(obj);
      });
      resolve(bookmarkArray);
    });
  });

  bookmarkList
    .then((response) => {
      // console.log({ response });
      res.render("bookmark", { bookmarkList: response, error: null });
    })
    .catch((error) => {
      console.log({ error });
      res.render("bookmark", { bookmarkList: [], errorMsg: error });
    });

  // res.render("bookmark");
});

router.get("/api/bookmark", async function (req, res, next) {
  const db = admin.database();
  const ref = db.ref("bookmark");
  const bookmarkList = new Promise((resolve, reject) => {
    var bookmarkArray = [];
    ref.once("value", async (snapshot) => {
      const data = await snapshot.val();
      Object.entries(data).map((item, index) => {
        let obj = {};
        obj.key = item[0];
        obj.title = item[1].data.linkTitle;
        obj.url = item[1].data.linkurl;
        console.log(obj);

        bookmarkArray.push(obj);
        // console.log(item)
        // console.log(`${index} => ${item}`);
      });
      resolve(bookmarkArray);
    });
  });

  bookmarkList
    .then((response) => {
      res.status(200).json({ bookmarkList: response, error: null });
    })
    .catch((error) => {
      res.status(400).json({ bookmarkList: [], errorMsg: error });
    });

  // res.render("bookmark");
});

module.exports = router;
