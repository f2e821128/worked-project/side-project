const express = require("express");
const fetch = require("node-fetch");

const fetchTitleMethod = require("../utils/fetchTitle");
const insertData = require("../utils/insertData");
const router = express.Router();

/* GET Manage Dashboard page. */
router.get("/manageDashboard", function (req, res, next) {
  res.render("manageDashboard");
});

router.post("/textUrl", function (req, res, next) {
  const { urltext } = req.body;
  console.log(req.body);

  console.log({ urltext });

  fetchTitleMethod(urltext)
    .then((result) => {
      insertData("bookmark", { linkTitle: result, linkurl: urltext });
      res.status(200).json({ msg: "done", error: "" });
    })
    .catch((err) => {
      console.log({ err });
      res.status(200).json({ msg: "error", error: err });
    });
});

module.exports = router;
